import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import NotesList from './components/NotesList'
import CreateNote from './components/CreateNote'
import Note from './containers/SelectedNote'

export default () => {
    return (
        <BrowserRouter>
          <Switch>
            <Route exact path="/"  component={NotesList} />
            <Route path="/create-note" component={CreateNote} />
            <Route path="/notes/:title" component={Note} />} />
          </Switch>
        </BrowserRouter>
    );
}
