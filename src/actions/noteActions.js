export function createNote(note) {
  return { type: 'CREATE_NOTE', note};
}

export function sortNotes(sortFlag) {
  return { type: 'SORT_NOTE', sortFlag};
}

export function deleteNote(link) {
  return { type: 'DELETE_NOTE', link};
}

export function searchNotes(term) {
  return { type: 'SEARCH_NOTES', term};
}
