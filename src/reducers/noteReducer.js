export default function noteReducer(state = [], action) {
  switch (action.type) {
    case 'CREATE_NOTE':
      return [...state,
        Object.assign({}, action.note)
      ];

    case 'SORT_NOTE':
      return [...state.sort( (a, b) => {
          if( a.timeInSeconds < b.timeInSeconds ) return action.sortFlag === 'ASC' ? -1 : 1;
          if( a.timeInSeconds > b.timeInSeconds ) return action.sortFlag === 'DESC' ? -1 : 1;
          return 0;
        })
      ];

    case 'DELETE_NOTE':
      return [...state.slice(0, state.findIndex(x => x.link==action.link)), ...state.slice(state.findIndex(x => x.link==action.link) + 1)]

    case 'SEARCH_NOTES':
      return state;

    default:
      return state
  }
}
