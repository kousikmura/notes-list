import React from 'react'
import {connect} from 'react-redux'
import Note from '../components/Note'
import * as noteActions from '../actions/noteActions'

const mapStateToProps = (state, ownProps) => {
  return {
    notes: state.notes
  }
}

const mapDispatchToProps = dispatch => {
  return {
    deleteNote: link => dispatch(noteActions.deleteNote(link))
  }
}

const SelectedNote = connect(mapStateToProps, mapDispatchToProps)(Note)

export default SelectedNote
