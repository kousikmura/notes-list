import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import Navigation from './Navigation'

class Note extends React.Component {
    onClickDelete() {
      this.props.deleteNote(this.props.match.params.title);
    }
    render() {
      return (
        <div className="app-conatiner">
            <Navigation />
            <div className="screen">
                {this.props.notes.map( (selectedNote, index) => {
                    if(this.props.match.params.title == selectedNote.link) {
                      return (
                        <div className="selected-note__wrapper">
                          <div className="selected-note__actions">
                            <span className="selected-note__delete" onClick={this.onClickDelete.bind(this)}>
                              <Link to="/" className="btn--primary">Delete</Link>
                            </span>
                          </div>
                          <h1 className="selected-note__title">{selectedNote.title}</h1>
                          <p className="selected-note__desc">{selectedNote.description}</p>
                          <p className="selected-note__time">{selectedNote.formattedTime}</p>
                        </div>
                      )
                    }
                  })
                }
            </div>
        </div>
      )
    }
}

Note.propTypes = {
    notes: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        link: PropTypes.string.isRequired,
        timeInSeconds: PropTypes.number.isRequired,
        formattedTime: PropTypes.string.isRequired
      }).isRequired
    ).isRequired,
    deleteNote: PropTypes.func.isRequired
}

export default Note
