import React from 'react'
import {connect} from 'react-redux'
import { Link } from 'react-router-dom'
import Navigation from './Navigation'
import * as noteActions from '../actions/noteActions'

class NotesList extends React.Component {
    onClickSort(sortFlag) {
      this.props.sortNote(sortFlag);
    }
    render() {
        return (
            <div className="app-conatiner">
                <Navigation />
                <div className="screen">
                    <div className="notes-list__actions">
                      <div className="sort-list">
                        <a className="sort-list__link btn--primary" onClick={() => this.onClickSort('ASC')}>Sort date asc</a>
                        <a className="sort-list__link btn--primary" onClick={() => this.onClickSort('DESC')}>Sort date desc</a>
                      </div>
                    </div>
                    <div className="notes-wrapper">
                      <ul className="notes-list">
                        {this.props.notes.map( (note, index) => {
                            return <li key={index} className="notes-list__item">
                                    <Link to={'notes/' + note.link} className="notes-list__link">
                                      <div className="notes-list__item-wrapper">
                                        <p className="notes-list__title">{note.title}</p>
                                        <p className="notes-list__desc">{note.description}</p>
                                        <p className="notes-list__time">{note.formattedTime}</p>
                                      </div>
                                    </Link>
                                  </li>
                          }
                        )}
                      </ul>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
  return {
    notes: state.notes
  }
}

const mapDispatchToProps = dispatch => {
  return {
    sortNote: sortFlag => dispatch(noteActions.sortNotes(sortFlag))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotesList)
