import React from 'react'
import Navigation from './Navigation'
import {connect} from 'react-redux'
import * as noteActions from '../actions/noteActions'
import { Link } from 'react-router-dom';

class CreateNote extends React.Component {

    onClickSave(e) {
      e.preventDefault()
      const title = this.titleNode.value
      const description = this.descriptionNode.value
      const link = title.split(' ').join('-')
      let d = new Date()
      const formattedTime = d.toDateString() + ', ' + d.getHours() + ':' + d.getMinutes()
      const timeInSeconds = d.getTime()
      const note = {
        title,
        description,
        link,
        formattedTime,
        timeInSeconds
      }
      this.props.createNote(note)
    }
    render() {
        return (
            <div className="app-conatiner">
                <Navigation />
                <div className="screen">
                  <form className="form">
                    <div className="form-field__wrapper">
                      <input type="text" name="title" className="form-field--text" placeholder="Enter note title" ref={node => (this.titleNode = node)} required/>
                    </div>
                    <div className="form-field__wrapper">
                      <textarea className="form-field--textarea" name="description" placeholder="Enter note description" ref={node => (this.descriptionNode = node)} required>
                      </textarea>
                    </div>
                    <div className="form-field__wrapper">
                      <span className="form-field__item" onClick={this.onClickSave.bind(this)}>
                        <Link className="btn--primary" to="/">Save</Link>
                      </span>
                    </div>
                  </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
  return {
    notes: state.notes
  }
}

const mapDispatchToProps = dispatch => {
  return {
    createNote: note => dispatch(noteActions.createNote(note))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateNote)
