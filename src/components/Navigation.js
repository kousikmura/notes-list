import React from 'react'
import { Link } from 'react-router-dom'

const Navigation = () => {
    return (
        <div className="navigation">
            <ul className="navigation__list">
              <li className="navigation__list-item"> <Link to="/">Home</Link> </li>
              <li className="navigation__list-item"> <Link to="/create-note">Create Note</Link> </li>
            </ul>
        </div>
    );
}

export default Navigation
