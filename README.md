# Notes

Single page application to create notes

# Features!

  - Display list of notes, persisted in local storage
  - Add a note to the list of notes
  - Delete a note
  - Sorting of notes by created time
  - Using Redux state management library to manage app state

### Tech stack used
 - React
 - Redux
 - ES6
 - SASS

### Dependencies


| Dependency | Version |
| ------ | ------ |
| React | 16.6.0 |
| Redux | 4.0.1 |
| React Router | 4.3.1 |
| Webpack | 4.25.1 |
| Babel | 6+ |

### Installation

Install the dependencies and start the application.

```sh
$ cd notes-list
$ npm install
$ npm start
```

Build to dist folder

```sh
$ npm run build
```

# Author
- Kousik Mura (mura.kousik@gmail.com)
